<?php


namespace Vallarj\Mezzio\OAuth\ResourceServer;


use Vallarj\Mezzio\OAuth\ResourceServer\Middleware\OAuthResourceServerMiddleware;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                \Ory\Hydra\Client\Api\AdminApi::class => Factory\Hydra\AdminApiFactory::class,
                OAuthResourceServerMiddleware::class => Factory\Middleware\OAuthResourceServerMiddlewareFactory::class,
            ],
        ];
    }
}
