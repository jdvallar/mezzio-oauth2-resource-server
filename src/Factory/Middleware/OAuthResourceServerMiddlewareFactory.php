<?php


namespace Vallarj\Mezzio\OAuth\ResourceServer\Factory\Middleware;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Vallarj\Mezzio\OAuth\ResourceServer\Middleware\OAuthResourceServerMiddleware;
use Ory\Hydra\Client\Api\AdminApi;

class OAuthResourceServerMiddlewareFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new OAuthResourceServerMiddleware($container->get(AdminApi::class));
    }
}
