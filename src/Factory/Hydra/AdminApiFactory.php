<?php


namespace Vallarj\Mezzio\OAuth\ResourceServer\Factory\Hydra;


use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\Configuration;

class AdminApiFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        if (!isset($config["hydra"]["admin"]["host"])) {
            throw new \Exception("Configuration key [hydra][admin][host] is missing.");
        }

        $hydraConfig = new Configuration();
        $hydraConfig->setHost($config["hydra"]["admin"]["host"]);

        return new AdminApi(null, $hydraConfig);
    }
}
