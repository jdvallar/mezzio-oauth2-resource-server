<?php


namespace Vallarj\Mezzio\OAuth\ResourceServer\Middleware;


use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\ApiException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

class OAuthResourceServerMiddleware implements MiddlewareInterface
{
    const USER_ID_ATTRIBUTE = 'oauth-user-id';
    const HAS_ACCESS_TOKEN_ATTRIBUTE = 'oauth-has-access-token';
    const ACCESS_TOKEN_VALID_ATTRIBUTE = 'oauth-valid-token';
    const SCOPES_ATTRIBUTE = 'oauth-scopes';

    /** @var AdminApi */
    private $hydraAdmin;

    /**
     * OAuthResourceServerMiddleware constructor.
     *
     * @param AdminApi $adminApi
     */
    public function __construct(AdminApi $adminApi)
    {
        $this->hydraAdmin = $adminApi;
    }

    /**
     * @inheritDoc
     * @throws ApiException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        // Get token from headers
        $tokenHeader = $request->getHeaderLine("Authorization");
        $token = trim(array_slice(preg_split("/Bearer\\s+/", $tokenHeader), -1)[0]);
        $hasAccessToken = $token !== "";

        if (!$hasAccessToken) {
            return $handler->handle(
                $request->withAttribute(self::HAS_ACCESS_TOKEN_ATTRIBUTE, false)
                    ->withAttribute(self::USER_ID_ATTRIBUTE, "")
                    ->withAttribute(self::ACCESS_TOKEN_VALID_ATTRIBUTE, false)
                    ->withAttribute(self::SCOPES_ATTRIBUTE, [])
            );
        }

        $result = $this->hydraAdmin->introspectOAuth2Token($token);
        $scopes = preg_split("/\\s+/", $result->getScope() ?? "");
        $userId = $result->getSub();
        $validAccessToken = $result->getActive();

        return $handler->handle(
            $request->withAttribute(self::HAS_ACCESS_TOKEN_ATTRIBUTE, $hasAccessToken)
                ->withAttribute(self::USER_ID_ATTRIBUTE, $userId)
                ->withAttribute(self::ACCESS_TOKEN_VALID_ATTRIBUTE, $validAccessToken)
                ->withAttribute(self::SCOPES_ATTRIBUTE, $scopes)
        );
    }
}
